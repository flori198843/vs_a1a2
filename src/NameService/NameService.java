package NameService;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.UnknownHostException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import message.Message;
import middleware.MoMNS;

public class NameService implements INameService {
	private String[][] binds = new String[10][3];
	private static String ownIP;

	public NameService(String ownIP) {
		for (int i = 0; i < 10; i++) {
			binds[i][0] = "";
			binds[i][1] = "";
		}
		this.ownIP = ownIP;
	}

	@Override
	public void bind(String name, String ip) {
		// Daten im Array speichern
		
		String[] value = name.split(",");
		
		for (int i = 0; i < 10; i++) {
			if (binds[i][0].equals("")) {
				binds[i][0] = value[0];
				binds[i][1] = ip;
				binds[i][2] = value[1];
				i = 10;
			}
		}
	}

	@Override
	public void unbind(String name) {
		String tmp;

		for (int i = 0; i < 10; i++) {
			tmp = binds[i][0];

			if (tmp.equals(name)) {
				binds[i][0] = "";
				binds[i][1] = "";
				binds[i][2] = "";
				i = 10;
			}
		}

	}

	@Override
	public void rebind(String name, String ip) {

		String tmp;

		for (int i = 0; i < 10; i++) {
			tmp = binds[i][0];

			if (tmp.equals(name)) {
				binds[i][1] = ip;
			}
		}

	}

	@Override
	public void lookup(String name, String ip, int port)
			throws SocketException, UnknownHostException, ParserConfigurationException, TransformerException {
		String tmp = "";
		String result = "";

		for (int i = 0; i < 10; i++) {
			tmp = binds[i][0];

			if (tmp.equals(name)) {
				result = binds[i][1];
				result += "," + binds [i][2];
			}
		}

		// Nachricht erstellen und senden
		Message msg = new Message(9, result.length(), result, ownIP, 2000, "Namer");
		System.out.println("Lookup: " + result);
		DatagramSocket ds = new DatagramSocket(1500);
		MoMNS mw = new MoMNS(ds);
		mw.send(msg, ip, port);
		ds.close();
	}

	public void getNames(String ip, int port) {
		String names = "";

		for (int i = 0; i < binds.length; i++) {
			if (i == 0) {
				names = binds[i][0];
			} else {
				names += "," + binds[i][0];
			}
		}

		// Nachricht erstellen und senden
		Message msg = new Message(10, names.length(), names, ownIP, 2000, "Namer");
		DatagramSocket ds;

		try {
			ds = new DatagramSocket(1700);
			MoMNS mw = new MoMNS(ds);
			mw.send(msg, ip, port);
			ds.close();
		} catch (SocketException | UnknownHostException | ParserConfigurationException | TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void printBinds() {
		for (int i = 0; i < binds.length; i++) {
			System.out.println(i + ": " + binds[i][0]);
			System.out.println(i + ": " + binds[i][1]);
		}
	}

	public static void main(String[] args)
			throws IOException, SAXException, ParserConfigurationException, TransformerException {
		DatagramSocket receiver = new DatagramSocket(2000);
		NameService naming = new NameService(args[0]);
		MoMNS mw = new MoMNS(receiver);
		Message request;
		int type;
		

		while (true) {
			// Nachrichten annehmen
			request = mw.receive();
			type = request.getType();

			switch (type) {
			case 6:
				System.out.println(request.valueToString());
				System.out.println(request.getSrcIP());
				naming.bind(request.valueToString(), request.getSrcIP());
				break;
			case 7:
				naming.unbind(request.valueToString());
				break;
			case 8:
				naming.rebind(request.valueToString(), request.getSrcIP());
				;
				break;
			case 9:
				naming.lookup(request.valueToString(), request.getSrcIP(), request.getSrcPORT());
				;
				break;
			case 10:
				naming.getNames(request.getSrcIP(), request.getSrcPORT());
				;
				break;
			default:
				System.out.println("Nothing to do");
				;
				break;
			}
		}
	}
}
