package NameService;

import java.net.SocketException;
import java.net.UnknownHostException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

public interface INameService 
{
	public void bind(String name, String ip);
	public void unbind(String name);
	public void rebind(String name, String ip);
	public void lookup(String name, String ip, int port) throws SocketException, UnknownHostException, ParserConfigurationException, TransformerException;
	public void getNames(String ip, int port);
	public void printBinds();
}
