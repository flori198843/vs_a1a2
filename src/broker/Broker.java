package broker;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.SocketException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import message.Message;
import middleware.IMoM;
import middleware.MoMNS;
import skeleton_pi.IRobot;
import skeleton_pi.Robot;

public class Broker {

	public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException {
		DatagramSocket ds = new DatagramSocket(3000);
		IMoM mw = new MoMNS(ds);
		
		IRobot robot = new Robot();
		
		while(1==1){
			Message msg = mw.receive();
			switch (msg.getType()){
				case 0: break;
				case 1: robot.moveHorizontal(Integer.parseInt((msg.valueToString())), msg.getHwID());
						break;
				case 2: break;
				case 3: break;
				case 4: break;
				case 5: break;
				case 6: break;
				case 7: break;
				case 8: break;
				case 9: break;
				case 10: break;
				
			}
		}
		
	}

}
