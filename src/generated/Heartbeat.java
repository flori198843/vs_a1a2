
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import message.Message;
import neo_middleware.MoMProxy;

public class Heartbeat implements Runnable {
	
	private InetAddress ia;
	private DatagramSocket ds;
	private MoMProxy mw;
	private String raspIP;
	private String ownIP;	
	private String ownName;
	private int basePort;
	
	public Heartbeat(String raspIP, String ownIP, String ownName, int basePort) {
		this.ownName = ownName;
		this.raspIP = raspIP;
		this.ownIP=ownIP;
		this.basePort = basePort;
		try {
			mw = new MoMProxy(basePort + 559,false,"Devran","",ownIP);
		} catch (SocketException | UnknownHostException | TransformerException | ParserConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
	}
	
	@Override
	public void run() {
		
		while (true) {
			
			try {
			
		
				String s = new String(RobotHorizontal.getInstanceOf().getCurrentHorizontalPercent()  
				+ "," + RobotVertical.getInstanceOf().getCurrentVerticalPercent() 
						+ "," + RobotGripper.getInstanceOf().isGripperClosed()  
						 	);					

				
				Message hbMsg = new Message(0,32,s,ownIP,basePort + 559,ownName);
				

				
				mw.send(hbMsg, raspIP, basePort + 554);
				
				Thread.sleep(1000);
			
			} catch (IOException e) {			
				e.printStackTrace();
			}  catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
}
