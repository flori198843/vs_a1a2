
//neoware
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Arrays;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import marshalling.Marshalling;
import message.Message;

public class MoM implements Runnable{
	DatagramSocket ds;
	private String ownIP;
	
	public MoM(int port, boolean bind, String name, String ownIP) throws SocketException, UnknownHostException, TransformerException, ParserConfigurationException {
		ds = new DatagramSocket(port);
		//ds.setSoTimeout(5000);
		System.out.println(ds.getSoTimeout());
		this.ownIP=ownIP;
		if(bind) {
			
			Message bindMsg = new Message(6,4,name,getMyIP(), port, name);
			sendToNS(bindMsg);
		}
	}
	
	public String getMyIP() throws UnknownHostException {
		return ownIP;
//		return InetAddress.getLocalHost().toString().substring(6);
	}
	
	public void sendToNS(Message msg) throws UnknownHostException, TransformerException, ParserConfigurationException {
		byte[] sendBuffer = msgToByteArr(msg);

		//Anlegen und senden des DatagramPackets
		InetAddress dest = InetAddress.getByName(getMyIP());
		DatagramPacket output = new DatagramPacket(sendBuffer, sendBuffer.length, dest, 2000);
			
		try {
			ds.send(output);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void send(Message msg, String name, int port) throws SocketException, UnknownHostException, ParserConfigurationException, TransformerException 
	{		
		String destIP = lookup(name);
		String[] value = destIP.split(",");
		
		System.out.println("DestiIP: "+ value[0]);
		System.out.println("DestiPort: "+ value[1]);
		
		byte[] sendBuffer = msgToByteArr(msg);

		//Anlegen und senden des DatagramPackets
		if(value[0].equalsIgnoreCase("")) {
			
		} else {
			InetAddress dest = InetAddress.getByName(value[0]);
			DatagramPacket output = new DatagramPacket(sendBuffer, sendBuffer.length, dest, Integer.parseInt(value[1]));
			
			System.out.println(dest);
			System.out.println(value[0]);
			System.out.println(value[1]);
			try {
				ds.send(output);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public String lookup(String name) throws UnknownHostException, TransformerException, ParserConfigurationException
	{
//		System.out.println(name);
		Message lookup = new Message(9, 32, name, getMyIP(), 4556, name);

		byte[] sendBuffer = msgToByteArr(lookup);
		
		//Anlegen und senden des DatagramPackets
		InetAddress dest = InetAddress.getByName(getMyIP()); //IP vom NS
		DatagramPacket output = new DatagramPacket(sendBuffer, sendBuffer.length, dest, 2000);
		
		try {
			ds.send(output);
			lookup = receive();
			
		} catch (IOException | SAXException | ParserConfigurationException e) {
			e.printStackTrace();
		}
		
//		System.out.println(lookup.getValue().toString());
		
		return lookup.getValue().toString();
	}
	
	public Message receive() throws IOException, SAXException, ParserConfigurationException 
	{
		Document doc = null;;
		byte[] receiveBuffer = new byte[1024];
		InetAddress source = null;
		int port = 0;
		
		//Paket empfangen
		DatagramPacket input = new DatagramPacket(receiveBuffer, receiveBuffer.length, source, port);
		
			ds.receive(input);

		

		
		
		//Array auf richtige laenge kuerzen
		byte[] test = trimByte(input.getData());
			
		//Byte Array in Document umwandeln
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		doc = builder.parse(new ByteArrayInputStream(test));
			
		return Marshalling.unmarshall(doc);
	}
	
	private byte[] msgToByteArr(Message msg) throws TransformerException, ParserConfigurationException {
		Document doc = Marshalling.marshall(msg);
		
		//Umwandeln des eingegebenen Dokuments in ein Byte Array
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		
		StreamResult result=new StreamResult(bos);
		transformer.transform(source, result);
		
		byte[] sendBuffer = new byte[1024];
		sendBuffer = bos.toByteArray();
		
		return sendBuffer;
	}
	
	public static byte[] trimByte(byte[] paddedArr) {
		int i = paddedArr.length - 1;
			while(i>=0 && paddedArr[i]==0) {
				--i;
			}
		return Arrays.copyOf(paddedArr, i + 1);
	}

	@Override
	public void run() {
		try {
			Message msg = receive();
		} catch (IOException | SAXException | ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//to-do
		
	}
	
}
