
//oldware
import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import message.*;

public interface IMoM 
{
	public void send(Message msg, String ip, int port) throws SocketException, UnknownHostException, ParserConfigurationException, TransformerException;
	public Message receive() throws IOException, SAXException, ParserConfigurationException;
}
