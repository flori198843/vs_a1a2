
public interface IRobot {

	public void moveHorizontal(int i, String location);

	public void moveVertical(int i, String location);

	public void grab(int i, String location);

	public int getVerticalPercent();
	
	public int getHorizontalPercent();
	
	public int getGrabStatus();
	
	public void currentRobot(String rbt);
	
	public boolean eStopPressed();
}
