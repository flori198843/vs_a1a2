
//neoware
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Arrays;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import marshalling.Marshalling;
import message.Message;

public class MoMProxy {
	DatagramSocket ds;
	
	public MoMProxy(int port, boolean bind, String name, String nsIP, String ownIP) throws SocketException, UnknownHostException, TransformerException, ParserConfigurationException {
		ds = new DatagramSocket(port);
		if(bind) {
			System.out.println(getMyIP());
			String value = name + "," + Integer.toString(port);
			//Message bindMsg = new Message(6,4,name,getMyIP(), port, name);
			Message bindMsg = new Message(6,4,value,ownIP, port, name);
			sendToNS(bindMsg, nsIP);
		}
	}
	
	public String getMyIP() throws UnknownHostException {
		return InetAddress.getLocalHost().getHostAddress();
	}
	
	public void sendToNS(Message msg,String nsIP) throws UnknownHostException, TransformerException, ParserConfigurationException {
		byte[] sendBuffer = msgToByteArr(msg);

		//Anlegen und senden des DatagramPackets
		InetAddress dest = InetAddress.getByName(nsIP); //NS ip
		DatagramPacket output = new DatagramPacket(sendBuffer, sendBuffer.length, dest, 2000);
			
		try {
			ds.send(output);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void send(Message msg, String ip, int port) throws SocketException, UnknownHostException, ParserConfigurationException, TransformerException 
	{		
	
		byte[] sendBuffer = msgToByteArr(msg);

		//Anlegen und senden des DatagramPackets
//		System.out.println(ip);
		InetAddress dest = InetAddress.getByName(ip);
		DatagramPacket output = new DatagramPacket(sendBuffer, sendBuffer.length, dest, port);
			
		try {
			ds.send(output);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	public Message receive() throws IOException, SAXException, ParserConfigurationException 
	{
		Document doc = null;;
		byte[] receiveBuffer = new byte[1024];
		InetAddress source = null;
		int port = 0;
		
		//Paket empfangen
		DatagramPacket input = new DatagramPacket(receiveBuffer, receiveBuffer.length, source, port);
		ds.receive(input);
		
		//Array auf richtige laenge kuerzen
		byte[] test = trimByte(input.getData());
			
		//Byte Array in Document umwandeln
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		doc = builder.parse(new ByteArrayInputStream(test));
			
		return Marshalling.unmarshall(doc);
	}
	
	private byte[] msgToByteArr(Message msg) throws TransformerException, ParserConfigurationException {
		Document doc = Marshalling.marshall(msg);
		
		//Umwandeln des eingegebenen Dokuments in ein Byte Array
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		
		StreamResult result=new StreamResult(bos);
		transformer.transform(source, result);
		
		byte[] sendBuffer = new byte[1024];
		sendBuffer = bos.toByteArray();
		
		return sendBuffer;
	}
	
	public static byte[] trimByte(byte[] paddedArr) {
		int i = paddedArr.length - 1;
			while(i>=0 && paddedArr[i]==0) {
				--i;
			}
		return Arrays.copyOf(paddedArr, i + 1);
	}
	
}
