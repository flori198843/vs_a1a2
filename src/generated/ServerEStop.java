
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import message.Message;
import middleware.IMoM;
import middleware.MoMNS;

public class ServerEStop implements Runnable {
	

	private DatagramSocket ds; 
	
	private IMoM mw;
	private String raspIP;
	private String ownIP;
	private int basePort;
	
	public ServerEStop(String raspIP, String ownIP, int basePort) {
		
		this.raspIP=raspIP;
		this.ownIP=ownIP;
		this.basePort = basePort;
		try {
			
			ds =  new DatagramSocket(basePort+558);
			mw = new MoMNS(ds);
		} catch (SocketException e) {			
			e.printStackTrace();
		}		
	}
	
	@Override
	public void run() {
		
		while (true) {
			
			try {
			
				Message msg = mw.receive();			
				try {
					RobotStop.getInstanceOf().eStop();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//Message ackMsg = new Message(11,32,"ACK",ownIP, basePort + 558,"HellaM#1");
				//mw.send(ackMsg, raspIP, basePort + 554);
				//System.out.println(msg.valueToString());
							
			//	byte[] b2 = String.valueOf(str2).getBytes();
					
			//	DatagramPacket dp1 = new DatagramPacket(b2, b2.length, ia, dp.getPort());
			//	ds.send(dp1);
			
			} catch (IOException e) {			
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
}
