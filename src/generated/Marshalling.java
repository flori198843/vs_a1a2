
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import message.Message;

public class Marshalling {

	public static Document marshall(Message m) throws ParserConfigurationException {
		
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		
		Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement("Message");
		doc.appendChild(rootElement);
		rootElement.setAttribute("type",Integer.toString(m.getType()));
		rootElement.setAttribute("length",Integer.toString(m.getLength()));
		rootElement.setAttribute("value",m.valueToString());
		rootElement.setAttribute("srcIP",m.getSrcIP());
		rootElement.setAttribute("srcPORT",Integer.toString(m.getSrcPORT()));
		rootElement.setAttribute("hwID",m.getHwID());
		
		//Element typeElement = doc.createElement("type");
		//rootElement.appendChild(typeElement);
		

		return doc;
	}

	public static Message unmarshall(Document doc) {
		String isMessage = doc.getDocumentElement().getNodeName();
		
		int type = Integer.parseInt(doc.getDocumentElement().getAttribute(("type")));
		int length = Integer.parseInt(doc.getDocumentElement().getAttribute(("length")));
		String srcIP = doc.getDocumentElement().getAttribute("srcIP");
		int srcPort = Integer.parseInt(doc.getDocumentElement().getAttribute("srcPORT"));
		String hwID = doc.getDocumentElement().getAttribute("hwID");
		
		int valInt = 0;
		byte valByte = 0;
		String valString = "";
		boolean valBoolean = false;
		
		if(!isMessage.contentEquals("Message")) {
			System.out.println("No Message!!");
			return null;
		}
		
		if(type<0 || type >11) {
			System.out.println("Unknown Type!!");
			return null;
		}
			
		switch(type) {
			
			case 1: case 2: case 3: case 4:
			case 5:	valByte = Byte.parseByte(doc.getDocumentElement().getAttribute(("value")));
					return new Message<Byte>(type,length,valByte,srcIP,srcPort,hwID);
			case 0: case 6: case 7: case 8: case 9: case 11:
			case 10: valString = doc.getDocumentElement().getAttribute(("value"));
					return new Message<String>(type,length,valString,srcIP,srcPort,hwID);
			case 1100: valInt = Integer.parseInt(doc.getDocumentElement().getAttribute(("value")));
					return new Message<Integer>(type,length,valInt,srcIP,srcPort,hwID);
			case 1200:  valBoolean = Boolean.getBoolean(doc.getDocumentElement().getAttribute(("value")));
					return new Message<Boolean>(type,length,valBoolean,srcIP,srcPort,hwID);
		}
		
		return null;
	}

	public static void main(String... strings) throws ParserConfigurationException {
		Message m1 = new Message(1,2,(byte) 2,"127.0.0.1",5555,"Hella#M1");
		Document d = marshall(m1);
		System.out.println(d.getDocumentElement().getNodeName());
		System.out.println(d.getDocumentElement().getAttribute("type"));
		System.out.println(d.getDocumentElement().getAttribute("length"));
		System.out.println(d.getDocumentElement().getAttribute("value"));
		System.out.println(d.getDocumentElement().getAttribute("srcIP"));
		System.out.println(d.getDocumentElement().getAttribute("srcPORT"));
		System.out.println(d.getDocumentElement().getAttribute("hwID"));
		
		
		Message m2 = unmarshall(d);
		System.out.println(m2.getType());
		System.out.println(m2.getLength());
		System.out.println(m2.getValue().getClass());
		System.out.println(m2.valueToString());
		System.out.println(m2.getSrcIP());
		System.out.println(m2.getSrcPORT());
		System.out.println(m2.getHwID());
		
		try {
			System.out.println(InetAddress.getLocalHost());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
	}
}
