package skeleton_pi;

public interface IRobot {

	public void moveHorizontal(int i, String location);

	public void moveVertical(int i, String location);

	public void grab(int i, String location);



}
