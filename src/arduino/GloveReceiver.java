package arduino;

import java.util.Arrays;

import com.fazecast.jSerialComm.SerialPort;

public class GloveReceiver implements Runnable{
	private boolean index;
	private boolean middle;
	private boolean ring;
	private boolean pinky;
	private boolean thumb;
	private int horizontal;
	private int vertical;
	private int grab;

	public GloveReceiver() {
	}
	
	public int getHorizontal() {
		return horizontal;
	}
	public int getVertical() {
		return vertical;
	}
	public int getGrab() {
		return grab;
	}
	public String toString() {
//		return "Thumb: " + thumb + "\n"+
//				"Index: " + index + "\n"+
//				"Middle: " + middle + "\n"+
//				"Ring: " + ring + "\n"+
//				"Pinky: " + pinky + "\n";
	
		return "H: " + horizontal + "\n"+
		"V: " + vertical + "\n"+
		"G: " + grab + "\n";
	}

	public static void main(String[] args) throws InterruptedException {
		GloveReceiver t = new GloveReceiver();
		Thread t2 = new Thread(t);
		t2.start();
		while(true) {
			Thread.sleep(1000);
			System.out.println(t.toString());
		}
	}

	@Override
	public void run() {
		SerialPort sp = SerialPort.getCommPort("COM5");
		sp.setComPortParameters(9600, 8, 0, 0); // default connection settings for Arduino
		byte[] buffer = new byte[10];
		int bytesToRead = 10;
		sp.openPort();

		while (true) {

			/**
			 * Send data over serial interface Protocol: 8 8 8 8 n * 16 16 8
			 * |--0xAF--|--Version--|--Type--|--Length--|--Payload--|--CRC--|--0xFE--|
			 *
			 * Fields with a length of over 8 bits send their data LSB first
			 */
			sp.readBytes(buffer, bytesToRead);
			byte fsr11 = buffer[0];
			byte fsr12 = buffer[1];
			int x1 = fsr11*256+fsr12;
			byte fsr21 = buffer[2];
			byte fsr22 = buffer[3];
			int x2 = fsr21*256+fsr22;
			byte fsr31 = buffer[4];
			byte fsr32 = buffer[5];
			int x3 = fsr31*256+fsr32;
			byte fsr41 = buffer[6];
			byte fsr42 = buffer[7];
			int x4 = fsr41*256+fsr42;
			byte fsr51 = buffer[8];
			byte fsr52 = buffer[9];
			int x5 = fsr51*256+fsr52;
			
			if(x1>150&&x1<500) {
				thumb=true;
				grab = 1;
			} else if(x1<0 && x1>500){
				
			} else {
				thumb=false;
				grab = 0;

			}
			
			if(x2>150&&x2<500) {
				index=true;
				horizontal++;
			} else if(x2<0 && x2>500){
				
			} else {
				index=false;
			}
			
			if(x3>150&&x3<500) {
				middle=true;
				horizontal--;
			} else if(x3<0 && x3>500){
				
			} else {
				middle=false;
			}
			
			if(x4>150&&x4<500) {
				ring=true;
				vertical++;
			} else if(x4<0 && x4>500){
				
			} else {
				ring=false;
			}
			
			if(x5>150&&x5<500) {
				pinky=true;
				vertical--;
			} else if(x5<0 && x5>500){
				
			} else {
				pinky=false;
			}
			if(horizontal>100) {
				horizontal=100;
			}
			if(horizontal<0) {
				horizontal=0;
			}
			if(vertical>100) {
				vertical=100;
			}
			if(vertical<0) {
				vertical=0;
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

}
