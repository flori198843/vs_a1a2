package proxy;

import java.awt.List;
import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import lejos.internal.io.SocketOutputStream;
import message.Message;
import neo_middleware.MoMProxy;

public class Proxy {
	static String horizontal ="0";
	static String vertical="0";
	static String grab="0";
	
	public static void main(String...strings ) throws IOException, SAXException, ParserConfigurationException, TransformerException {
		MoMProxy mw;
		String robotIP = strings[0];
		String nsIP = strings[2];
		int basePort = Integer.parseInt(strings[5]);

		mw = new MoMProxy(basePort + 554,true,strings[1],nsIP, strings[4]);
			ArrayList<String> clientIPs = new ArrayList<String>();
			clientIPs.add(strings[3]);
			
		
			
			
			
		while(true) {
			Message msg = mw.receive();
			System.out.println(msg.getType());
			System.out.println(msg.getSrcIP());
			System.out.println(msg.valueToString());
//			if(!equalsRobotIP(msg.getSrcIP(),robotIP)){
				//clientIPs.add(msg.getSrcIP());
//			}
			
			switch (msg.getType()) {
				case 0: 
					
					/*String tmpString = handle(msg);
					Message tmpMsg = new Message(0,32,tmpString,msg.getSrcIP(),msg.getSrcPORT(),msg.getHwID());*/
					for(String s: clientIPs) {
					mw.send(msg, s, 4555);
				}
						break;	
				case 1: mw.send(msg, robotIP, basePort + 555);
				break;	
				case 2: mw.send(msg, robotIP, basePort + 556);
				break;	
				case 3: mw.send(msg, robotIP, basePort + 557);
				break;
				case 4: mw.send(msg, robotIP, basePort + 558);
				break;	
				case 11:
					for(String s: clientIPs) {
					mw.send(msg, s, 4556);
				}
				break;
				default: break;
			}
		}
	}
	
	public static boolean equalsRobotIP(String other, String robotIP) {
		return other.equalsIgnoreCase(robotIP);
	}
	
	private static String handle(Message msg){
		if(msg.getSrcPORT()==5555){
			horizontal = msg.valueToString();
		} else if(msg.getSrcPORT()==5556){
			vertical = msg.valueToString();
		} else {
			grab = msg.valueToString();
		}
		return horizontal+","+vertical+","+grab;
	}
	
	
	
}
