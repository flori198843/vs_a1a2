package middleware;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import message.*;
import marshalling.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Arrays;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import javax.xml.transform.dom.DOMSource;




public class MoMNS implements IMoM{
	
	private DatagramSocket ds;
	
	public MoMNS(DatagramSocket ds)
	{
		this.ds = ds;
	}
	
	@Override
	public void send(Message msg, String ip, int port) throws SocketException, UnknownHostException, ParserConfigurationException, TransformerException 
	{	
		
		Document doc = Marshalling.marshall(msg);
		
		//Umwandeln des eingegebenen Dokuments in ein Byte Array
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		
		StreamResult result=new StreamResult(bos);
		transformer.transform(source, result);
		
		byte[] sendBuffer = new byte[1024];
		sendBuffer = bos.toByteArray();
		
		//Anlegen und senden des DatagramPackets
		InetAddress dest = InetAddress.getByName(ip);
		DatagramPacket output = new DatagramPacket(sendBuffer, sendBuffer.length, dest, port);
		
		//Testausgaben
/*	    String te = new String(output.getData());
	    System.out.println(te);
		System.out.println(sendBuffer.length);*/
		
		try {
			ds.send(output);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Message receive() throws IOException, SAXException, ParserConfigurationException 
	{
		Document doc = null;;
		byte[] receiveBuffer = new byte[1024];
		InetAddress source = null;
		int port = 0;
		
		//Paket empfangen
		DatagramPacket input = new DatagramPacket(receiveBuffer, receiveBuffer.length, source, port);
		ds.receive(input);
		
		//Array auf richtige laenge kuerzen
		byte[] test = trimByte(input.getData());
			
		//Byte Array in Document umwandeln
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		doc = builder.parse(new ByteArrayInputStream(test));
		
		//Source IP und Port erfassen
		//sourceIP = input.getAddress().toString().substring(1);
		//sourcePort = input.getPort();
		
		//Zum Testen
		//System.out.println("IP:" + input.getAddress());
		//System.out.println("Port:" + input.getPort());
			
		return Marshalling.unmarshall(doc);
	}
	
	
	public static byte[] trimByte(byte[] paddedArr) {
		int i = paddedArr.length - 1;
			while(i>=0 && paddedArr[i]==0) {
				--i;
			}
		return Arrays.copyOf(paddedArr, i + 1);
	}

}
