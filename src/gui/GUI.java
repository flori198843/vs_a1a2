package gui;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.UnknownHostException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.cads.ev3.gui.ICaDSRobotGUIUpdater;
import org.cads.ev3.gui.swing.CaDSRobotGUISwing;
import org.cads.ev3.rmi.consumer.ICaDSRMIConsumer;
import org.cads.ev3.rmi.generated.cadSRMIInterface.IIDLCaDSEV3RMIMoveGripper;
import org.cads.ev3.rmi.generated.cadSRMIInterface.IIDLCaDSEV3RMIMoveHorizontal;
import org.cads.ev3.rmi.generated.cadSRMIInterface.IIDLCaDSEV3RMIMoveVertical;
import org.cads.ev3.rmi.generated.cadSRMIInterface.IIDLCaDSEV3RMIUltraSonic;
import org.xml.sax.SAXException;

import arduino.GloveReceiver;
import message.Message;
import stub_client.IRobot;
import stub_client.Robot;
import middleware.IMoM;
import middleware.MoMNS;
import neo_middleware.MoM;

public class GUI implements Runnable,IIDLCaDSEV3RMIMoveGripper, IIDLCaDSEV3RMIMoveHorizontal,
IIDLCaDSEV3RMIMoveVertical, IIDLCaDSEV3RMIUltraSonic, ICaDSRMIConsumer {
	
	private int updateH;
	private int updateV;
	private String currentRobot = "";
	
	//gui = new CaDSRobotGUISwing(consumer, gripper, vertical, horizontal, ultraSonic);
	CaDSRobotGUISwing gui = new CaDSRobotGUISwing(this,this,this,this,this);
	
	static IRobot legoRobot;
	static private GloveReceiver glove;
	
	
	
	
	@Override
	public void run() {	
		gui.startGUIRefresh(100);		
		gui.addService("HellaM#1");
		gui.addService("Devran");
		gui.addService("Flo");
		
		Thread move = null;
		
			move = new Thread() {

				public void run() {
					while (true) {
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.println(glove.toString());
						try {
							if(!legoRobot.eStopPressed()) {
//							if((glove.getVertical()<legoRobot.getVerticalPercent()-2)||(glove.getVertical()>legoRobot.getVerticalPercent()+2)) {
//								moveVerticalToPercent(0,glove.getVertical());
								legoRobot.moveVertical(glove.getVertical(),currentRobot);

//							}
//							if((glove.getHorizontal()<legoRobot.getHorizontalPercent()-2)||(glove.getHorizontal()>legoRobot.getHorizontalPercent()+2)) {
//								moveHorizontalToPercent(0,glove.getHorizontal());
								legoRobot.moveHorizontal(glove.getHorizontal(),currentRobot);

//							}
//							if(glove.getGrab()!=legoRobot.getGrabStatus()) {
								if(glove.getGrab()==1) {
									openGripper(1);
								} else {
									closeGripper(0);
								}
//							}
							}
							

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
				}

			};
		 
		move.start();

	}
		
	
	
	public static void main(String[] args) throws SocketException {
		legoRobot = new Robot(args[0]);
		(new Thread(new GUI())).start();
		
		glove = new GloveReceiver();
		Thread t2 = new Thread(glove);
		t2.start();
		
	}
	
	@Override
	public int moveVerticalToPercent(int arg0, int arg1) throws Exception {
		//System.out.println(arg1);
		legoRobot.moveVertical(arg1,currentRobot);
		return 0;
	}
	
	@Override
	public int moveHorizontalToPercent(int arg0, int arg1) throws Exception {
		legoRobot.moveHorizontal(arg1,currentRobot);
		return 0;
	}
	
	@Override
	public int openGripper(int arg0) throws Exception {
		legoRobot.grab(0,currentRobot);
		return 0;
	}
	
	@Override
	public int closeGripper(int arg0) throws Exception {
		legoRobot.grab(1,currentRobot);
		return 0;
	}
	
	@Override
	public int getCurrentVerticalPercent() throws Exception {
		
		return legoRobot.getVerticalPercent();
	}

	@Override
	public int getCurrentHorizontalPercent() throws Exception {
		return legoRobot.getHorizontalPercent();
	}	

	@Override
	public int stop(int arg0) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	
	
	@Override
	public void register(ICaDSRobotGUIUpdater arg0) {
	
	}

	@Override
	public void update(String arg0) {
		currentRobot = arg0;
		legoRobot.currentRobot(currentRobot);

		System.out.println(arg0);
	}

	@Override
	public int isUltraSonicOccupied() throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}





	@Override
	public int isGripperClosed() throws Exception {
		// TODO Auto-generated method stub
//		System.out.println(legoRobot.getGrabStatus());
		return legoRobot.getGrabStatus();
	}



}
