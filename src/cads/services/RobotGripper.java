package cads.services;

import java.util.concurrent.atomic.AtomicInteger;

import org.cads.ev3.middleware.CaDSEV3RobotHAL;
import org.cads.ev3.rmi.generated.cadSRMIInterface.IIDLCaDSEV3RMIMoveGripper;

public class RobotGripper implements IIDLCaDSEV3RMIMoveGripper{

	private static AtomicInteger TID = new AtomicInteger(0);
	private Listener lg = Listener.getInstance();
	private static RobotGripper instance;
	
	//private ErrWatchdog ewd = new ErrWatchdog();
	//private Thread tewd = new Thread(ewd);
	
	private RobotGripper () {
		//tewd.start();
	}
	
	public static synchronized RobotGripper getInstanceOf() {
		if (instance != null){
					
		} else {
			instance = new RobotGripper();
		}		
		return instance;
	}
	
	@Override
	public int closeGripper(int transactionID) throws Exception {		
		
		CaDSEV3RobotHAL.getInstance().doClose();
		System.out.println("Close.... TID: " + TID.get());			
		//System.out.println("Close Gripper: " + isGripperClosed());
		
		return TID.incrementAndGet();
	}

	@Override
	public int isGripperClosed() throws Exception {				
		return lg.getGripperStatus();
	}

	public void toggleGripper(int transactionID, int i) throws Exception {
		if(i==1) {
			System.out.println("closing");
			closeGripper(transactionID);
		} else if(i==0){
			System.out.println("opening");
			openGripper(transactionID);
		}
	}
	
	@Override
	public int openGripper(int transactionID) throws Exception {		
		
		CaDSEV3RobotHAL.getInstance().doOpen();
		System.out.println("Open.... TID: " + TID.get());
		//System.out.println("Open Gripper: " + isGripperClosed());
		
		return TID.incrementAndGet();	
	}

}
