package cads.services;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import message.Message;
import middleware.IMoM;
import middleware.MoMNS;

public class ServerVertical implements Runnable {
	
	
	private DatagramSocket ds; 
	private IMoM mw;
	private String raspIP;
	private String ownIP;
	private int basePort;
	
	public ServerVertical(String raspIP, String  ownIP, int basePort) {
		this.raspIP=raspIP;
		this.ownIP=ownIP;
		this.basePort = basePort;
		try {
			
			ds =  new DatagramSocket(basePort+556);
			mw = new MoMNS(ds);
		} catch (SocketException e) {			
			e.printStackTrace();
		}		
	}
	
	@Override
	public void run() {
		
		while (true) {
			
			try {
				Message msg = mw.receive();		
				//Message ackMsg = new Message(11,32,"ACK",ownIP, basePort + 556,"HellaM#1");
				//mw.send(ackMsg, raspIP, basePort + 554);
				RobotVertical.getInstanceOf().moveVerticalToPercent(0, Integer.parseInt(msg.valueToString()));
			
			} catch (IOException e) {			
				e.printStackTrace();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
}
