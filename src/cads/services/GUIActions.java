package cads.services;


import org.cads.ev3.gui.ICaDSRobotGUIUpdater;
import org.cads.ev3.rmi.consumer.ICaDSRMIConsumer;
import org.cads.ev3.rmi.generated.cadSRMIInterface.IIDLCaDSEV3RMIUltraSonic;

public class GUIActions implements  IIDLCaDSEV3RMIUltraSonic, ICaDSRMIConsumer {	
		
	Listener l = Listener.getInstance();	
		
	private static GUIActions instance;
	
	private GUIActions() {
		
	}	
	
	public static synchronized GUIActions getInstanceOf() {
		if (instance != null) {
		} else {
			instance = new GUIActions();
		}
		return instance;
	}

	@Override
	public void register(ICaDSRobotGUIUpdater observer) {
		 System.out.println("New Observer");
	     observer.addService("Service 1");
	     observer.addService("Service 2");
	     observer.setChoosenService("Service 2", -1, -1, false);
		
	}

	@Override
	public void update(String comboBoxText) {
		System.out.println("Combo Box updated " + comboBoxText);		
	}

	@Override
	public int isUltraSonicOccupied() throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

//	@Override
//	public int getCurrentVerticalPercent() throws Exception {
//		return l.getVerticalPercent();
//	}
//
//	@Override
//	public int moveVerticalToPercent(int transactionID, int percent) throws Exception {			
//		Thread move = new Thread() {
//		    public void run() {	
//		    	apv.setParamVerticalPercent(percent);	    		    	
//		    	try {
//					if(getCurrentVerticalPercent() < percent) {	
//						apv.setMoveUp(1);		
//						GUIActions.stopVerticalOk = true;
//						CaDSEV3RobotHAL.getInstance().moveUp();
//					} else if (getCurrentVerticalPercent() > percent) {	
//						apv.setMoveUp(0);	
//						GUIActions.stopVerticalOk = true;
//						CaDSEV3RobotHAL.getInstance().moveDown();						
//					} else {
//											
//					}						
//					
//				} catch (Exception e) {					
//					e.printStackTrace();
//				}
//		    }
//		};			
//		move.start();
//		
//		TID.incrementAndGet();
//					
//		System.out.println("Call to move vertical -  TID: " + TID.get() + " degree " + percent);
//		
//		return EID;
//	}
//
//	@Override
//	public int getCurrentHorizontalPercent() throws Exception {		
//		return l.getHorizontalPercent();
//	}
//
//	@Override
//	public int moveHorizontalToPercent(int transactionID, int percent) throws Exception {
//					
//		Thread move = new Thread() {
//		    public void run() {
//		    	aph.setParamHorizontalPercent(percent);	    	
//		    	try {
//					if(getCurrentHorizontalPercent() < percent) {
//						GUIActions.stopHorizontalOk = true;
//						CaDSEV3RobotHAL.getInstance().moveLeft();						
//					} else if (getCurrentHorizontalPercent() > percent) {
//						GUIActions.stopHorizontalOk = true;
//						CaDSEV3RobotHAL.getInstance().moveRight();						
//					} else {
//												
//					}				
//					
//				} catch (Exception e) {					
//					e.printStackTrace();
//				}
//		    }
//		};			
//		move.start();
//	
//		TID.incrementAndGet();		
//			
//		System.out.println("Call to move horizontal -  TID: " + TID.get() + " degree " + percent);
//				
//		return EID;
//	}
//
//	@Override
//	public int stop(int transactionID) throws Exception {
//							
//		CaDSEV3RobotHAL.getInstance().stop_h();
//		CaDSEV3RobotHAL.getInstance().stop_v();
//		GUIActions.stopHorizontalOk = false;
//		GUIActions.stopVerticalOk = false;		
//				
//		return transactionID;
//	}
//
//	@Override
//	public int closeGripper(int transactionID) throws Exception {		
//							
//			TID.incrementAndGet();
//			CaDSEV3RobotHAL.getInstance().doClose();
//			System.out.println("Close.... TID: " + TID.get());			
//		
//		return EID;
//	}
//
//	@Override
//	public int isGripperClosed() throws Exception {
//		// TODO Auto-generated method stub
//		return EID;
//	}
//
//	@Override
//	public int openGripper(int transactionID) throws Exception {
//		
//		TID.incrementAndGet();
//		CaDSEV3RobotHAL.getInstance().doOpen();
//		System.out.println("Open.... TID: " + TID.get());
//		
//		return EID;
//	}
//	
//	public static int stopVertical() throws Exception {
//									
//			CaDSEV3RobotHAL.getInstance().stop_v();	
//			GUIActions.stopVerticalOk = false;		
//			System.out.println("Position ok, stopped vertical movement!");			
//		
//		return EID;
//	}
//	
//	public static int stopHorizontal() throws Exception {
//		
//		CaDSEV3RobotHAL.getInstance().stop_h();
//		GUIActions.stopHorizontalOk = false;
//		System.out.println("Position ok, stopped horizontal movement!");
//		
//		return EID;
//	}
//	
//	public synchronized static void setEID() {
//		EID--;		
//	}
//	
//	public static boolean getStopVerticalOk() {		
//		return stopVerticalOk;
//	}
//	
//	public static boolean getStopHorizontalOk() {		
//		return stopHorizontalOk;
//	}
}
