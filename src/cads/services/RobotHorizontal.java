package cads.services;

import java.util.concurrent.atomic.AtomicInteger;

import org.cads.ev3.middleware.CaDSEV3RobotHAL;
import org.cads.ev3.rmi.generated.cadSRMIInterface.IIDLCaDSEV3RMIMoveHorizontal;

public class RobotHorizontal implements IIDLCaDSEV3RMIMoveHorizontal {

	private Listener lh = Listener.getInstance();
	private static volatile boolean stopHorizontalOk;
	private static AtomicInteger TID = new AtomicInteger(0);
	private static int EID = 0;
	private static RobotHorizontal instance;
	
	private ActualPositionHorizontal aph = new ActualPositionHorizontal();
	private Thread taph = new Thread(aph);
	
	private RobotHorizontal() {
		taph.start();
	}
	
	public static synchronized RobotHorizontal getInstanceOf() {
		if (instance != null){
			
		} else {
			instance = new RobotHorizontal();
		}
	return instance;
	}
	
	@Override
	public int getCurrentHorizontalPercent() throws Exception {		
		return lh.getHorizontalPercent();		
	}

	@Override
	public int moveHorizontalToPercent(int transactionID, final int percent) throws Exception {
		Thread move = new Thread() {
		    public void run() {
		    	ErrWatchdog.resetCounter();
		    	CaDSEV3RobotHAL.getInstance().stop_h();
		    	aph.setParamHorizontalPercent(percent);	    	
		    	try {
					if(getCurrentHorizontalPercent() < percent) {
						RobotHorizontal.stopHorizontalOk = true;
						CaDSEV3RobotHAL.getInstance().moveLeft();						
					} else if (getCurrentHorizontalPercent() > percent) {
						RobotHorizontal.stopHorizontalOk = true;
						CaDSEV3RobotHAL.getInstance().moveRight();						
					} else {
												
					}				
					
				} catch (Exception e) {					
					e.printStackTrace();
				}
		    }
		};			
		move.start();
	
		TID.incrementAndGet();		
			
		System.out.println("Call to move horizontal -  TID: " + TID.get() + " degree " + percent);
				
		return EID;		
	}

	@Override
	public int stop(int transactionID) throws Exception {
		
		CaDSEV3RobotHAL.getInstance().stop_h();
		return TID.incrementAndGet();
	}
	
	public static int stopHorizontal() throws Exception {
		
		CaDSEV3RobotHAL.getInstance().stop_h();	
		RobotHorizontal.stopHorizontalOk = false;			
		
		return TID.incrementAndGet();
	}
	
	public static boolean getStopHorizontalOk() {		
		return stopHorizontalOk;
	}

}
