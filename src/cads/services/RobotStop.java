package cads.services;

public class RobotStop {

	private static RobotStop instance;
		
	private RobotStop() {
		
	}
	
	public synchronized static RobotStop getInstanceOf() {
		
		if(instance != null) {						
		} else {			
			instance = new RobotStop();			
		}
		
		return instance;	
	}
	
	public void eStop() throws Exception {
		RobotVertical.stopVertical();
		RobotHorizontal.stopHorizontal();	
		System.out.println("Called eStop!");
	}
	
}
