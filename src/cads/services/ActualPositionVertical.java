package cads.services;

public class ActualPositionVertical implements Runnable {

	private Listener l1 = Listener.getInstance();	
	private static boolean running = true;
	private int verticalPercent;
	
	private int paramVerticalPercent = -1;
	private int moveUp;
	
	@Override
	public void run() {
		while(running) {
			
			if (RobotVertical.getStopVerticalOk()) {				
				//System.out.println("hier");				
				if (moveUp == 1) {					
					if (verticalPercent >= paramVerticalPercent) {						
						try {
							RobotVertical.stopVertical();
							System.out.println("Position ok, stopped vertical movement!");	
							paramVerticalPercent = -1;
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}					
					} else {
						verticalPercent = l1.getVerticalPercent();
					}
										
				} else {
					
					if ( verticalPercent <= paramVerticalPercent) {
						try {
							RobotVertical.stopVertical();
							System.out.println("Position ok, stopped vertical movement!");	
							paramVerticalPercent = -1;
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} 
						} else {
							verticalPercent = l1.getVerticalPercent();										
					}				
				}
			} else {
				
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}			
	}
	
	public static void terminate() {
		running = false;
	}
	
	public void setParamVerticalPercent(int percent) {		
		paramVerticalPercent = percent;
	}
	
	public void setMoveUp(int moveUp) {		
		this.moveUp = moveUp;
	}	
}
