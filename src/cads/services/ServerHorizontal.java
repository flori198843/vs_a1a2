package cads.services;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Arrays;

import message.Message;
import middleware.IMoM;
import middleware.MoMNS;

public class ServerHorizontal implements Runnable {
	
	
	private DatagramSocket ds;
	
	private IMoM mw;
	private String raspIP;
	private String ownIP;
	private int basePort;
	private Message ackMsg;
	
	public ServerHorizontal(String raspIP, String  ownIP, int basePort) {
		//Message ackMsg = new Message(11,32,"ACK",ownIP, basePort + 555,"HellaM#1");
		this.raspIP=raspIP;
		this.ownIP=ownIP;
		this.basePort = basePort;
		try {
			
			ds =  new DatagramSocket(basePort+555);
			mw = new MoMNS(ds);
		} catch (SocketException e) {			
			e.printStackTrace();
		}		
	}
	
	@Override
	public void run() {
		
		while (true) {
			
			try {
			
				
				Message msg = mw.receive();			
				//mw.send(ackMsg, raspIP, basePort + 554);
				RobotHorizontal.getInstanceOf().moveHorizontalToPercent(0, Integer.parseInt(msg.valueToString()));
			
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
}
