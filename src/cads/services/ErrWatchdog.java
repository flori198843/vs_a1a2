package cads.services;

public class ErrWatchdog implements Runnable {
	
	private int lastHorizontalPosition;
	private int lastVerticalPosition;
	private static boolean running = true;
	private static int counter = 0;
			
	@Override
	public void run() {
		while(running) {			
			if (RobotHorizontal.getStopHorizontalOk() || RobotVertical.getStopVerticalOk()) {
				
				try {
					
					lastHorizontalPosition = RobotHorizontal.getInstanceOf().getCurrentHorizontalPercent();
					
					lastVerticalPosition = RobotVertical.getInstanceOf().getCurrentVerticalPercent();
					
					for (int i = counter; i < 15; i++) {						
						Thread.sleep(100);						
					}						
					
					if(RobotHorizontal.getStopHorizontalOk() && lastHorizontalPosition == RobotHorizontal.getInstanceOf().getCurrentHorizontalPercent() 
							|| RobotVertical.getStopVerticalOk() && lastVerticalPosition == RobotVertical.getInstanceOf().getCurrentVerticalPercent()) {
																	
						System.out.println("Stop, jetzt reicht es!"); 
						RobotStop.getInstanceOf().eStop();
					} 
						
			} catch (Exception e1) {
				
				e1.printStackTrace();
		
		} //while	
			
		} else {
			
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	}
	
	public static void resetCounter() {
		counter = 0;		
	}
		
	public static void terminate() {
		running = false;		
	}
}
