package cads.services;

import org.cads.ev3.gui.swing.CaDSRobotGUISwing;
import org.cads.ev3.middleware.CaDSEV3RobotHAL;
import org.cads.ev3.middleware.CaDSEV3RobotType;
import org.cads.ev3.*;
import cads.services.*;


public class Simulation {
		
	private static CaDSRobotGUISwing gui;
	private static RobotHorizontal robotHorizontal = RobotHorizontal.getInstanceOf();
	private static RobotVertical robotVertical = RobotVertical.getInstanceOf();
	private static RobotGripper robotGripper = RobotGripper.getInstanceOf();
				
	/*
	 * Declare SocketObjects (Runnable)
	 */
	
	
	//public PISimulation piSimu = new PISimulation();	
	

	
	/*
	 * Give them to Threads
	 */
	
	
	public void test() {
		
		CaDSEV3RobotHAL.createInstance(CaDSEV3RobotType.SIMULATION, Listener.getInstance(), Listener.getInstance());
		
		GUIActions ga = GUIActions.getInstanceOf();
		
		gui = new CaDSRobotGUISwing(ga,robotGripper,robotVertical,robotHorizontal, ga);

		gui.startGUIRefresh(120);
				
	}
	
	//private Thread tPiSimu = new Thread(piSimu);		
	
//		public void test(String raspIP, String ownIP, String ownName, String mode, String basePort) {
//			
//			if(mode.equalsIgnoreCase("REAL")){
//			CaDSEV3RobotHAL.createInstance(CaDSEV3RobotType.REAL, Listener.getInstance(), Listener.getInstance());
//			} else {
//				CaDSEV3RobotHAL.createInstance(CaDSEV3RobotType.SIMULATION, Listener.getInstance(), Listener.getInstance());
//
//			}
//			
//			int b = Integer.parseInt(basePort);
//			
//			Heartbeat hb = new Heartbeat(raspIP, ownIP, ownName,b);
//			 ServerHorizontal sh = new ServerHorizontal(raspIP, ownIP,b);
//			 ServerVertical sv = new ServerVertical(raspIP, ownIP,b);
//			 ServerGripper sg = new ServerGripper(raspIP, ownIP,b);	
//			 ServerEStop es = new ServerEStop(raspIP, ownIP,b);
//			
//			 Thread thb = new Thread(hb);
//			 Thread tsh = new Thread(sh);
//			 Thread tsv = new Thread(sv);
//			 Thread tsg = new Thread(sg);
//			 Thread tes = new Thread(es);
//			
//			tsh.start();
//			tsv.start();
//			tsg.start();
//			tes.start();	
//			
//		//	tPiSimu.start();	
//			thb.start();		
//
////			GUIActions ga = GUIActions.getInstanceOf();
////						
////			gui = new CaDSRobotGUISwing(ga,robotGripper,robotVertical,robotHorizontal, ga);
////			
////			//gui.startGUIRefresh(120);
//												
//		}	
	
	public static void main (String args[]) {
		
		Simulation hauptSimu = new Simulation();
				
		//hauptSimu.test(args[0], args[1], args[2], args[3], args[4]);
		
		hauptSimu.test();
		
	
		
//		while (true) {
//			
//			try {
//				Thread.sleep(2000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			System.out.printf("##### Information from Heartbeat ##### " + "%n" + PISimulation.strh);
//			
//		}
	}
}
