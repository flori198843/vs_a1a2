package cads.services;

import java.util.concurrent.atomic.AtomicInteger;

import org.cads.ev3.middleware.CaDSEV3RobotHAL;
import org.cads.ev3.rmi.generated.cadSRMIInterface.IIDLCaDSEV3RMIMoveVertical;

public class RobotVertical implements IIDLCaDSEV3RMIMoveVertical {
	
	private Listener lv = Listener.getInstance();
	private static volatile boolean stopVerticalOk;
	private static AtomicInteger TID = new AtomicInteger(0);
	private static int EID = 0;
	private static RobotVertical instance;
	private static Listener l1 = Listener.getInstance();
	
	private ActualPositionVertical apv = new ActualPositionVertical();
	private Thread tapv = new Thread(apv);
	
	private RobotVertical() {
		tapv.start();
	}
	
	public static synchronized RobotVertical getInstanceOf() {
		if (instance != null){
			
		} else {
			instance = new RobotVertical();
		}
	return instance;
	}

	@Override
	public int getCurrentVerticalPercent() throws Exception {
		return lv.getVerticalPercent();		
	}

	@Override
	public int moveVerticalToPercent(int transactionID, final int percent) throws Exception {				
		Thread move = new Thread() {
		    public void run() {	
		    	
		    	System.out.println("Rest: " + getRestValue (percent));
		    	
		    	ErrWatchdog.resetCounter();
		    	CaDSEV3RobotHAL.getInstance().stop_v();
		    	apv.setParamVerticalPercent(percent);	    		    	
		    	try {
					if(getCurrentVerticalPercent() < percent) {	
						if (percent - getCurrentVerticalPercent() >= 4) {
							apv.setMoveUp(1);		
							RobotVertical.stopVerticalOk = true;
							CaDSEV3RobotHAL.getInstance().moveUp();							
						}
						
					} else if (getCurrentVerticalPercent() > percent && getRestValue (percent) <= 4) {	
						if (percent - getCurrentVerticalPercent() >= 4) {
							apv.setMoveUp(0);	
							RobotVertical.stopVerticalOk = true;
							CaDSEV3RobotHAL.getInstance().moveDown();	
						}
					} else {
											
					}						
					
				} catch (Exception e) {					
					e.printStackTrace();
				}
		    }
		};		
	
			move.start();			
		
		TID.incrementAndGet();
					
		System.out.println("Call to move vertical -  TID: " + TID.get() + " degree " + percent);
		
		return EID;
	}
	
	public int getRestValue (int paramVerticalPercent) {
		
		int rest = 0;
		
		System.out.println("paramVerticalPercent: "+ paramVerticalPercent +  " l1.getVerticalPercent() " + l1.getVerticalPercent());
		
		if (l1.getVerticalPercent() == 0) {
			
		} else {				
			
			if (l1.getVerticalPercent() < paramVerticalPercent) {
				
				rest = (int) (paramVerticalPercent) % (int) (l1.getVerticalPercent());
				
			} else {
				
				rest =  (int) (l1.getVerticalPercent()) % (int) (paramVerticalPercent);
				
			}			
			
		}
		
		return rest;
		
	}

	@Override
	public int stop(int transactionID) throws Exception {
		
		CaDSEV3RobotHAL.getInstance().stop_v();
		return TID.incrementAndGet();
	}
	
	public static int stopVertical() throws Exception {
		
		CaDSEV3RobotHAL.getInstance().stop_v();	
		RobotVertical.stopVerticalOk = false;					
		
		return TID.incrementAndGet();
	}
	
	public static boolean getStopVerticalOk() {		
		return stopVerticalOk;
	}
}
