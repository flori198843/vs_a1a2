package cads.services;

public class ActualPositionHorizontal implements Runnable {

	private Listener l1 = Listener.getInstance();
	private static boolean running = true;
	private int horizontalPercent;
		
	private int paramHorizontalPercent;
		
	@Override
	public void run() {
		while(running) {				
		
			if (RobotHorizontal.getStopHorizontalOk()) {				
				if (horizontalPercent == paramHorizontalPercent) {
					try {
						RobotHorizontal.stopHorizontal();
						System.out.println("Position ok, stopped horizontal movement!");	
						paramHorizontalPercent = -1;
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}					
					//System.out.println("Reached position, stop horizontal movement!");					
				} else {
					horizontalPercent = l1.getHorizontalPercent();
				}
			} else {
				
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}																					
				
		 }
	 }
	
	public static void terminate() {
		running = false;
	}
	
	public void setParamHorizontalPercent(int percent) {		
		paramHorizontalPercent = percent;
	}
}
