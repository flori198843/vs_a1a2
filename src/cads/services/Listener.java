package cads.services;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.cads.ev3.middleware.CaDSEV3RobotHAL;
import org.cads.ev3.middleware.CaDSEV3RobotType;
import org.cads.ev3.middleware.hal.ICaDSEV3RobotFeedBackListener;
import org.cads.ev3.middleware.hal.ICaDSEV3RobotStatusListener;
import org.json.simple.JSONObject;

public class Listener implements ICaDSEV3RobotStatusListener, ICaDSEV3RobotFeedBackListener  {
		
	private int horizontalPercent;
	private int verticalPercent;
	private Lock horizontalLock = new ReentrantLock();
	private Lock verticalLock = new ReentrantLock();
	private Lock gripperLock = new ReentrantLock();
	private static Listener instance = null;
	private String gripperInfo = "open";	
			
	private Listener() {
		
						
	}

public static synchronized Listener getInstance() {
	
	if (instance == null) {
		instance = new Listener();		
	} else {
				
	}
	return instance;
}
	
@Override
public void giveFeedbackByJSonTo(JSONObject feedback) {
	//System.out.println(feedback);
	
}

@Override
public void onStatusMessage(JSONObject status) {
	
	//System.out.println(status);	
	
	int percent = 0;
	
	String state = (String) status.get("state");
	
	Long percentLong = (Long) status.get("percent");		
	
	if(percentLong!=null){
		percent = percentLong.intValue();		
	}
	
	if (state.equalsIgnoreCase("horizontal")) {
		horizontalLock.lock();
		horizontalPercent = percent;
		horizontalLock.unlock();		
	} else if (state.equalsIgnoreCase("vertical")) {
		verticalLock.lock();
		verticalPercent = percent;
		verticalLock.unlock();	 
	} else if (state.equalsIgnoreCase("gripper")) {
		gripperLock.lock();
		gripperInfo = (String) status.get("value");
		gripperLock.unlock();
	}
}

public int getHorizontalPercent() {	
	int percent;
	horizontalLock.lock();	
	percent = horizontalPercent;
	horizontalLock.unlock();
	return percent;
}

public int getVerticalPercent() {	
	int percent;
	verticalLock.lock();	
	percent = verticalPercent;
	verticalLock.unlock();
	return percent;
}

public int getGripperStatus() {	
	int tmp = -1;	
	try {
		Thread.sleep(100);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	if(gripperInfo.equalsIgnoreCase("open")){
		tmp = 0;		
	} else {
		tmp = 1;
	}			
	return tmp;	
}

}