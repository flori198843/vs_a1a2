package cads.services;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

public class PISimulation implements Runnable {
	
	private DatagramSocket ds;	
	private DatagramSocket dsh;	
	private InetAddress ia;	
	
	private Scanner i;
	private Scanner j;
	
	private final int portHorizontal = 5555;
	private final int portVertical = 5556;
	private final int portGripper = 5557;
	private final int portEStop = 5558;
	private final int portHeartbeat = 5559;
	
	public static String strh;
	
	public PISimulation() {		
		
		try {		
			 ds = new DatagramSocket();	
			 dsh = new DatagramSocket();
			 ia = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
		
	}

	@Override
	public void run() {
		
		while (true) {
			
			try {
				
				i = new Scanner(System.in);				
				int servicePort = portHorizontal;
				int param = 0;				
				System.out.print("Welcher Service: ");
				
				byte whichService = i.nextByte();
				
				switch (whichService) {
		            case 1:  servicePort = portHorizontal;
		            		 j = new Scanner(System.in);	
		            		 System.out.print("Parameter: ");
		            		 param = j.nextByte();
		            		 if (param < 0 || param > 100) {		            			
		            			 param = -1;
		            		 }
		                     break;
		            case 2:  servicePort = portVertical;		            		
		            		 j = new Scanner(System.in);	
		            		 System.out.print("Parameter: ");	
		            		 param = j.nextByte();
		            		 if (param < 0 || param > 100) {		            			 
		            			 param = -1;
		            		 }
		                     break;
		            case 3:  servicePort = portGripper;
		            		 j = new Scanner(System.in);	
		            		 System.out.print("Parameter: ");
		            		 param = j.nextByte();
		            		 if (param < 0 || param > 1) {		            			
		            			 param = -1;
		            		 }
		                     break;
		            case 4:  servicePort = portEStop;
		            		 j = new Scanner(System.in);	
		            		 System.out.print("Parameter: ");
		            		 param = j.nextByte();
		            		 if (param < 0 || param > 1) {		            			 
		            			 param = -1;
		            		 }
		                     break;	         
		            default: System.out.println("Invalid number"); 
		            		 param = -2;	
		                     break;
				}				
				
				if (param == -1) {
					System.out.println("Error: Wrong parameter value!");	
				} else if (param == -2) {
					System.out.println("Valid numbers: 1 2 3 4");					
				} else {
					
					byte[] b = String.valueOf(param).getBytes();		
					
					byte[] bhs = new String("").getBytes();
					
					byte[] bh = new byte[1024];
					
					DatagramPacket dp = new DatagramPacket(b, b.length, ia, servicePort);
					
					DatagramPacket dph = new DatagramPacket(bhs, bhs.length, ia, portHeartbeat);
					
					DatagramPacket dph1 = new DatagramPacket(bh, bh.length);
					
					ds.send(dp);
					dsh.send(dph);
				
					byte[] b1 = new byte[1024];
					DatagramPacket dp1 = new DatagramPacket(b1, b1.length);
					ds.receive(dp1);
					
					dsh.receive(dph1);
					
					String str = new String(dp1.getData());
					
					System.out.println("result is: " + str);	
					
					strh = new String(dph1.getData());					
					
				}			
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
						
		}		
	}	

}
