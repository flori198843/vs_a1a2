package stub_client;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.cads.ev3.middleware.CaDSEV3RobotHAL;
import org.xml.sax.SAXException;

import cads.services.RobotHorizontal;
import message.Message;
import neo_middleware.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Robot implements IRobot {

	private MoM mw;
	private int getVerticalPercent=0;
	private int getHorizontalPercent=0;
	private int getGrabStatus=1;
	private String name;
	private String currentRobot;
	private String ownIP;
	private boolean eStopPressed=false;
		
	private JFrame f; 
	private Message eStopMsg;

	
	
	public Robot(String ownIP) throws SocketException {
		currentRobot = "";
		name = "";
		this.ownIP=ownIP;
		try {
			mw = new MoM(4556, false, "",ownIP);
			
			 f = new JFrame("E-Stop");
			 f.setSize(300,400); 		 
			 f.setVisible(true);
			 
			 JButton b=new JButton("Stop movement!");
			 b.setBounds(100,100,140, 40); 
			 f.add(b);	
			 
			 eStopMsg = new Message(4, 8, (byte) 1, ownIP, 5555, "Client");
			 
			 b.addActionListener(new ActionListener() {
			        
					@Override
					public void actionPerformed(ActionEvent arg0) {
							try {
								mw.send(eStopMsg, "HellaM#1", 6555);
								eStopPressed=true;
							} catch (SocketException | UnknownHostException | ParserConfigurationException
									| TransformerException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} ;		
							}        
			      });
			 
			 
			
		} catch (UnknownHostException | TransformerException | ParserConfigurationException | SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
							
		 f = new JFrame("E-Stop");
		 f.setSize(300,400); 		 
		 f.setVisible(true);
		 
		 JButton b2=new JButton("Continue");
		 b2.setBounds(100,100,140, 40); 
		 f.add(b2);			
		 eStopMsg = new Message(4, 8, (byte) 1, ownIP, 5555, "Client");
		 
		 b2.addActionListener(new ActionListener() {
		        
				@Override
				public void actionPerformed(ActionEvent arg0) {
						
							eStopPressed=false;
						
						}      
		      });	
		
		
		Thread move = null;
		try {
			move = new Thread() {
				MoM mwRcv = new MoM(4555, false, "","");

				public void run() {
					while (true) {
						try {
							Message msg = mwRcv.receive();
							if (currentRobot.equalsIgnoreCase(msg.getHwID())) {
								if (msg.getType() == 0) {
									String tmp[] = msg.valueToString().split(",");
									getHorizontalPercent = Integer.parseInt(tmp[0]);
									getVerticalPercent = Integer.parseInt(tmp[1]);
									getGrabStatus = Integer.parseInt(tmp[2]);
									name = msg.getHwID();
								}
							} 
						} catch (IOException | SAXException | ParserConfigurationException e) {
							getHorizontalPercent = 0;
							getVerticalPercent = 0;
							getGrabStatus = 0;
							e.printStackTrace();
						}
					}
				}

			};
		} catch (UnknownHostException | TransformerException |

		ParserConfigurationException e)

		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		move.start();

	}
	
	public boolean eStopPressed() {
		return eStopPressed;
	}
	
	public void moveHorizontal(int i, String location) {

		Message msg = new Message(1, 32, (byte) i, ownIP, 5555, "Client");
		
		try {
			System.out.println(location);
			mw.send(msg, location, 6555);
			//Message ackRcv = mw.receive();
			

		} catch (ParserConfigurationException | TransformerException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(i);
	}

	public void moveVertical(int i, String location) {

		

		Message msg = new Message(2, 32, (byte) i, ownIP, 5555, "Client");

		try {
			mw.send(msg, location, 6555);
			//Message ackRcv = mw.receive();
			
		} catch ( ParserConfigurationException | TransformerException |  IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(i);
	}

	public void grab(int i, String location) {
		

		Message msg = new Message(3, 32, (byte) i, ownIP, 5555, "Client");
		
		try {
			mw.send(msg, location, 6555);
			//Message ackRcv = mw.receive();
			
		} catch (ParserConfigurationException | TransformerException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(i);
	}

	@Override
	public int getVerticalPercent() {
		// TODO Auto-generated method stub
		return getVerticalPercent;
	}

	@Override
	public int getHorizontalPercent() {
		// TODO Auto-generated method stub
		return getHorizontalPercent;
	}

	@Override
	public int getGrabStatus() {
		// TODO Auto-generated method stub
		return getGrabStatus;
	}

	public String getRobotName() {
		return name;
	}

	@Override
	public void currentRobot(String rbt) {
		currentRobot = rbt;
	}

}
