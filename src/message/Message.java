package message;

public  class Message<T> {
	private int type;
	private int length;
	private T value;
	private String srcIP;
	private int srcPORT;
	private String hwID;


	public Message(int type, int length, T value, String srcIP, int srcPORT, String hwID) {
		this.type = type;
		this.length = length;
		this.value=value;
		this.srcIP = srcIP;
		this.srcPORT = srcPORT;
		this.hwID = hwID;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public T getValue() {
		return value;
	}
	
	public void setValue(T value) {
		this.value=value;
	}
	
	public String valueToString() {
		return value.toString();
	}

	public String getSrcIP() {
		return srcIP;
	}

	public void setSrcIP(String srcIP) {
		this.srcIP = srcIP;
	}

	public int getSrcPORT() {
		return srcPORT;
	}

	public void setSrcPORT(int srcPORT) {
		this.srcPORT = srcPORT;
	}

	public String getHwID() {
		return hwID;
	}

	public void setHwID(String hwID) {
		this.hwID = hwID;
	}
	
}
