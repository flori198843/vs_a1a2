package idl_parser;

public class IDLBlueprint {
	
	private String functionName;
	private String returnType;
	private String parameterType;
	
	
	
	public IDLBlueprint(String r, String f, String p) {
		returnType = r;
		functionName = f;
		parameterType = p;
	}



	public String getFunctionName() {
		return functionName;
	}



	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}



	public String getReturnType() {
		return returnType;
	}



	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}



	public String getParameterType() {
		return parameterType;
	}



	public void setParameterType(String parameter) {
		this.parameterType = parameter;
	}
	
	@Override
	public String toString() {
		return "funcName: " + functionName + "\n" + "retType: " + returnType + "\n" + "paramType: " + parameterType;
	}
}
