package idl_parser;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.simple.parser.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class IDLParser {

	public static void main(String[] args) throws SAXException, ParserConfigurationException, IOException {
		System.out.println("#############################");
		System.out.println("Start - IDLParser");
		System.out.println();
		
		// Read IDL Example
		String fileName = "src/idl/idl.xml";
		System.out.println("::: read IDL from file: " + fileName + " :::");
		
		File file = new File("src/idl/idl.xml");
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.parse(file);
		
		document.getDocumentElement().normalize();
		
		System.out.println("Root element :" + document.getDocumentElement().getNodeName());
		
		NodeList functionList = document.getElementsByTagName("function");
		System.out.println("----------------------------");
		
		//list with idlfunctions
		List<IDLBlueprint> idlList = new ArrayList();
		
		for (int i = 0; i < functionList.getLength(); i++) {

			Node nNode = functionList.item(i);
			System.out.println("\nCurrent Element :" + nNode.getNodeName());
					
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {

				Element eElement = (Element) nNode;

				String functionName = eElement.getElementsByTagName("name").item(0).getTextContent();
				String returnType = eElement.getElementsByTagName("returnType").item(0).getTextContent();
				String parameterType = eElement.getElementsByTagName("parameterType").item(0).getTextContent();
				
		
				idlList.add(createIDLBlueprint(returnType,functionName,parameterType));
				
		
			}
		}
		
		String plainTextInterface = readEntirefile("src/plain_text/idl_interface.txt");
		String plainTextClass = readEntirefile("src/plain_text/idl_class.txt");
		String plainTextInterfaceFunction = readEntirefile("src/plain_text/idl_interface_function.txt");
		String plainTextClassFunction = readEntirefile("src/plain_text/idl_class_function.txt");
		
		String packageName = "generated";
		String className = "Robot";
		String interfaceName = "IRobot";
		
		System.out.println("---------------------");

		String interfaceString = createInterface(idlList,plainTextInterface,plainTextInterfaceFunction, interfaceName,packageName);
		System.out.println(interfaceString);
		writeToFile("src/generated/",interfaceName,interfaceString);
		
		String classString = createClass(idlList,plainTextClass,plainTextClassFunction,className,packageName);
		System.out.println(classString);
		writeToFile("src/generated/",className,classString);
		
		String serverHorizontal = readEntirefile("src/plain_text/ServerHorizontal.txt");
		System.out.println(serverHorizontal);
		writeToFile("src/generated/","ServerHorizontal",serverHorizontal);
		String serverVertical = readEntirefile("src/plain_text/ServerVertical.txt");
		System.out.println(serverVertical);
		writeToFile("src/generated/","ServerVertical",serverVertical);
		String serverGripper = readEntirefile("src/plain_text/ServerGripper.txt");
		System.out.println(serverGripper);
		writeToFile("src/generated/","ServerGripper",serverGripper);
		String serverEStop = readEntirefile("src/plain_text/ServerEStop.txt");
		System.out.println(serverEStop);
		writeToFile("src/generated/","ServerEStop",serverEStop);
		String heartbeat = readEntirefile("src/plain_text/Heartbeat.txt");
		System.out.println(heartbeat);
		writeToFile("src/generated/","Heartbeat",heartbeat);
		String iRobot = readEntirefile("src/plain_text/IRobot.txt");
		System.out.println(iRobot);
		writeToFile("src/generated/","IRobot",iRobot);
		String robot = readEntirefile("src/plain_text/Robot.txt");
		System.out.println(robot);
		writeToFile("src/generated/","Robot",robot);
		String marshall = readEntirefile("src/plain_text/Marshalling.txt");
		System.out.println(marshall);
		writeToFile("src/generated/","Marshalling",marshall);
		String message = readEntirefile("src/plain_text/Message.txt");
		System.out.println(message);
		writeToFile("src/generated/","Message",message);
		String iMom = readEntirefile("src/plain_text/IMoM.txt");
		System.out.println(iMom);
		writeToFile("src/generated/","IMoM",iMom);
		String momNs = readEntirefile("src/plain_text/MoMNS.txt");
		System.out.println(momNs);
		writeToFile("src/generated/","MoMNS",momNs);
		String mom = readEntirefile("src/plain_text/MoM.txt");
		System.out.println(mom);
		writeToFile("src/generated/","MoM",mom);
		String momProxy = readEntirefile("src/plain_text/MoMProxy.txt");
		System.out.println(momProxy);
		writeToFile("src/generated/","MoMProxy",momProxy);
	}
	
	
	
	private static String createInterface(List<IDLBlueprint> idlList, String plainTextInterface, String plainTextInterfaceFunction, String interfaceName, String packageName) {
		
		String interfaceFuntions = "";
		
		for(IDLBlueprint bp: idlList) {
			
			String s = "\t" + plainTextInterfaceFunction.replaceFirst("%s", bp.getReturnType()).replaceFirst("%s", bp.getFunctionName()).replaceFirst("%s", bp.getParameterType() + " i") + "\n";
			interfaceFuntions = interfaceFuntions.concat(s);
		}
		
		System.out.println(interfaceFuntions);
		
		String interfaceString = plainTextInterface.replaceFirst("%s",packageName).replaceFirst("%s", interfaceName).replaceFirst("%s", interfaceFuntions);
		
		return interfaceString;
	}
	
	private static String createClass(List<IDLBlueprint> idlList, String plainTextClass, String plainTextClassFunction, String className, String packageName) {
		
		String classFunctions = "";
		
		for(IDLBlueprint bp: idlList) {
			
			String s = "\t" +plainTextClassFunction.replaceFirst("%s", bp.getReturnType()).replaceFirst("%s", bp.getFunctionName()).replaceFirst("%s", bp.getParameterType() + " i") + "\n";
			s = s.replaceFirst("%s", "System.out.println();");
			classFunctions = classFunctions.concat(s);
		}
		
		System.out.println(classFunctions);
		
		String interfaceString = plainTextClass.replaceFirst("%s",packageName).replaceFirst("%s", className).replaceFirst("%s", classFunctions);
		
		return interfaceString;
	}
	
	private static IDLBlueprint createIDLBlueprint(String functionName, String returnType, String parameterType) {
		return new IDLBlueprint(functionName,returnType,parameterType);
	}
	
	private static String readEntirefile(String fileName) throws FileNotFoundException, IOException {
        BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));

        String line = "";
        StringBuffer buffer = new StringBuffer();
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
            buffer.append("\n");
        }

        reader.close();
        String returnText = buffer.toString();
        return returnText;
    }
	
	
	
	private static void writeToFile(String path, String objectName, String toWriteString) throws IOException {
        String fileName;
        fileName = path + objectName + ".java";
        PrintWriter writer = new PrintWriter(new FileWriter(new File(fileName)));

        writer.print(toWriteString);
        writer.flush();
        writer.close();
    }
	
}
